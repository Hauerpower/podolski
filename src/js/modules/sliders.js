export default function slidersInit() {

	(function ($) {

		// $('#home-slider').slick({
		// 	loop: true,
		// 	dots: false,
		// 	// autoplay: true,
		// 	infinite: true,
		// 	slidesToShow: 3,
		// 	slidesToScroll: 1,
		// 	arrows: false,
		// 	centerPadding:'50px'


		// 	// responsive: [
		// 	// 	{
		// 	// 		breakpoint: 800,
		// 	// 		settings: {
		// 	// 			slidesToShow: 1,
		// 	// 			infinite: true
		// 	// 		}
		// 	// 	}
		// 	// ]

		// });

		//slider on front_page
		$(".owl-1").owlCarousel({
			responsive: {
				0: {
					items: 2,
					stagePadding: 0,
					margin: 20
				},
				640: {
					items: 1,
					stagePadding: 30,
					margin: 20
				},
				700: {
					items: 1,
					stagePadding: 70,
					margin: 20
				},
				980: {
					items: 2,
					stagePadding: 20,
					margin: 20
				},
				1100: {
				    items: 2,
					stagePadding: 50,
					margin: 30,
				},
				1360: {
					items: 2,
					stagePadding: 70,
					margin: 30,
				},
				1600: {
					items: 3,
					stagePadding: 20,
					margin: 30,
				}
			},

			loop: true,
	
			autoplay: 2000,
			stopOnHover: true,
			smartSpeed: 1000,
			slideTransition: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
			dots: false,
			autoHeight: true
		});

		///slider uslugi

		$(".slider-big").owlCarousel({
			items:1,

			loop: true,
			autoplay: 2000,
			stopOnHover: true,
			smartSpeed: 1000,
			slideTransition: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
			dots: true,
			autoHeight: true
		});



	})(jQuery);

}