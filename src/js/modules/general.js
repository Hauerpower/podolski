export default function generalInit(){
	(function($){
	
		function myScrollTo( $target, $trigger ){
			if( $target.length ) {
				var offTop = $target.position().top;
				
				if( $trigger.attr('data-ignore-header') === undefined ){ 
					offTop -= $('#site-header').outerHeight();
					offTop -= 40;
				}
				
				var animationTime = Math.abs( offTop - $(window).scrollTop() ) * 1.2;
				
				$('html, body').stop().animate( {scrollTop: offTop}, animationTime );
			}
		}
		
		$('a[href="#"]').click(function(e){
			e.preventDefault();
		});
		
		$('a[data-scroll-down]').click(function(e){
			
			var $target = $( $(this).attr('href') );
			if( !$target.length ) $target = $(this).closest('section').next('section');
			
			if( $target.length ) e.preventDefault();
			
			myScrollTo($target, $(this) );
			
		});
	
		//add simple support for background images:
		$(document).on('lazybeforeunveil', function(e){
	    var bg = $(e.target).attr('data-bg');
	    if(bg){
	      e.target.style.backgroundImage = 'url(\'' + bg + '\')';
	    }
		});
		
	})(jQuery);
}